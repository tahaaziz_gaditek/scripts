from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

conf = SparkConf().setAppName("ARPU_taha")
sc = SparkContext(conf=conf)
spark = SparkSession.builder.config("hive.metastore.uris",
                                    "thrift://hmaster:9084").enableHiveSupport().getOrCreate()


db = {
    'hmaster': {
        'host': '172.16.1.15',
        'user': 'root',
        'password': 'gaditek',
        'port':  3306,
        'database': 'taha_test'
    }
}


spark.sql("use customerprofitability")
query = "select vpn_username, billing_cycle, tags, first_invoice_date_paid from bl_onboarding_flag"

df = spark.sql(query)

df.show()

# write data to another db
JDBC_CS_URL = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(db['hmaster'].get('host'), db['hmaster'].get(
    'port'), db['hmaster'].get('database'), db['hmaster'].get('user'), db['hmaster'].get('password'))

df \
    .write.format("jdbc") \
    .options(url=JDBC_CS_URL, dbtable='billing_tags', driver="com.mysql.jdbc.Driver")\
    .mode("overwrite") \
    .save()
