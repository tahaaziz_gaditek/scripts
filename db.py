import mysql.connector as mysql
from config import configuration


class DB:
    def __init__(self, db_name):
        db_conf = configuration.get(db_name)
        self.db = mysql.connect(**db_conf)

    def execute(self, query):
        try:
            query = query.lstrip()
            cursor = self.db.cursor(dictionary=True)
            cursor.execute(query)
            if query[0:6].lower() == 'select' or query[0:4].lower() == 'call' or query[0:4].lower() == 'show c':
                result  = cursor.fetchall()
            elif query[0:6].lower() == 'insert' or query[0:6].lower() == 'update' or query[0:6].lower() == 'delete':
                result = cursor.rowcount
            self.db.commit()
            cursor.close()
            return result
        except mysql.Error as e:
           raise

    def row(self, query):
        try:
            cursor = self.db.cursor()
            cursor.execute(query, multi=True)
            result = cursor.fetchone()
            return result
        except Exception as e:
            raise

    def close(self):
        self.db.close()

    def callSp(self, procedure, *args):
        try:
            cursor = self.db.cursor(dictionary=True)
            cursor.callproc(procedure, args)
            response = list()
            for result in cursor.stored_results():
                response.append(result.fetchall())
            self.db.commit()
            cursor.close()
            return response
        except mysql.Error as e:
           raise 
