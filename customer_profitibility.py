from db import DB
from datetime import date as dd, datetime as dt, timedelta
from config import configuration


class CustomerProfitability:
    def __init__(self):
        pass

    def getBillingData(self, **params):
        """Required(start_date, end_date)"""
        if params and params.get('start_date') is not None:
            start_date = params.get('start_date')
            end_date = (None, params.get('end_date'))[
                params.get('end_date') is not None]
        else:
            raise Exception("Invalid Number of Arguments were sent")

        db = DB('billing_read_only')

        if end_date is not None:

            query = """SELECT 
                    DATE(i.datepaid) AS date_paid,
                    TRIM(LOWER(v.`username`)) AS username,
                    i.id AS invoice_id,
                    ifnull (CASE WHEN LOWER(TRIM(h.billingcycle)) = 'one time'
                    THEN d.customBillingCycle
                    ELSE LOWER(TRIM(h.billingcycle))
                    END, 'N/A') AS billing_cycle,
                    CASE
                        WHEN cfv.relid = i.id 
                        THEN 1 
                        ELSE 0 
                    END AS is_upgraded,
                    it.amount,
                    IFNULL(ats.`affiliate_commission`, 0) AS CPA,
                    afe.Channel,
                    IF(
                        ats.affiliate_channel IS NULL 
                        OR ats.affiliate_channel = '',
                        'N/A',
                        ats.affiliate_channel
                    ) AS affiliate_channel 
                    FROM
                    tblinvoices i 
                    JOIN tblinvoiceitems it 
                        ON it.`invoiceid` = i.`id` 
                        AND it.`type` = 'Hosting' 
                    JOIN tblvpnusernames v 
                        ON v.`hosting_id` = it.relid 
                    JOIN tblaccounts a 
                        ON a.invoiceid = i.id
                    JOIN tblhosting h 
                    ON h.id = it.relid 
                    LEFT JOIN affiliate_transactions ats 
                        ON ats.invoice_id = i.id 
                    LEFT JOIN affiliatesEmail afe 
                        ON afe.`affiliateid` = ats.`affiliate_id` 
                    LEFT JOIN tblcustomfieldsvalues AS cfv 
                        ON i.id = cfv.`relid` 
                        AND cfv.`fieldid` = 28 
                        AND cfv.`relid` <> 0 
                    LEFT JOIN dpt_custom_billingcycle d 
                    ON h.packageid = d.relid
                    WHERE i.`status` = 'Paid' 
                    AND i.datepaid <> '0000-00-00 00:00:00' 
                    AND i.datepaid IS NOT NULL 
                    AND i.datepaid between  '{0}' and '{1}'
                    AND h.`domainstatus` IN ('Active', 'Cancelled')
                    AND it.`amount` > 0""" \
                .format(start_date, end_date)
        else:
            query = """SELECT 
                    DATE(i.datepaid) AS date_paid,
                    TRIM(LOWER(v.`username`)) AS username,
                    i.id AS invoice_id,
                    ifnull (CASE WHEN LOWER(TRIM(h.billingcycle)) = 'one time'
                    THEN d.customBillingCycle
                    ELSE LOWER(TRIM(h.billingcycle))
                    END, 'N/A') AS billing_cycle,
                    CASE
                        WHEN cfv.relid = i.id 
                        THEN 1 
                        ELSE 0 
                    END AS is_upgraded,
                    it.amount,
                    IFNULL(ats.`affiliate_commission`, 0) AS CPA,
                    afe.Channel,
                    IF(
                        ats.affiliate_channel IS NULL 
                        OR ats.affiliate_channel = '',
                        'N/A',
                        ats.affiliate_channel
                    ) AS affiliate_channel 
                    FROM
                    tblinvoices i 
                    JOIN tblinvoiceitems it 
                        ON it.`invoiceid` = i.`id` 
                        AND it.`type` = 'Hosting' 
                    JOIN tblvpnusernames v 
                        ON v.`hosting_id` = it.relid 
                    JOIN tblaccounts a 
                        ON a.invoiceid = i.id
                    JOIN tblhosting h 
                    ON h.id = it.relid 
                    LEFT JOIN affiliate_transactions ats 
                        ON ats.invoice_id = i.id 
                    LEFT JOIN affiliatesEmail afe 
                        ON afe.`affiliateid` = ats.`affiliate_id` 
                    LEFT JOIN tblcustomfieldsvalues AS cfv 
                        ON i.id = cfv.`relid` 
                        AND cfv.`fieldid` = 28 
                        AND cfv.`relid` <> 0 
                    LEFT JOIN dpt_custom_billingcycle d 
                    ON h.packageid = d.relid
                    WHERE i.`status` = 'Paid' 
                    AND i.datepaid <> '0000-00-00 00:00:00' 
                    AND i.datepaid IS NOT NULL 
                    AND date(i.datepaid) = '{0}'
                    AND h.`domainstatus` IN ('Active', 'Cancelled')
                    AND it.`amount` > 0""" \
            .format(start_date)

        billing_data = db.execute(query)

        if billing_data is not None:
            insertStatement = self.createBulkInsert(data=billing_data)

            db.close()
            db = DB('arpu')
            query = "INSERT INTO affiliate_cpa_raw(\
                            date_paid,\
                            username,\
                            billing_cycle, \
                            invoice_id,\
                            is_upgraded,\
                            amount,\
                            cpa,\
                            channel,\
                            affiliate_channel,\
                            added_on\
                            )\
                        VALUES {} ".format(insertStatement)
            rows = db.execute(query)
            db.close()
        return rows

    def createBulkInsert(self, **params):
        insert_list = list()
        data = params.get('data')
        for record in data:
            sql = """( "{0}", {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, "{9}" )""" \
                .format(
                    record.get('date_paid'), \
                    repr(record.get('username')), \
                    repr(record.get('billing_cycle')), \
                    record.get('invoice_id'), \
                    record.get('is_upgraded'), \
                    record.get('amount'), \
                    record.get('CPA'), \
                    repr(record.get('Channel')), \
                    repr(record.get('affiliate_channel')), \
                    dt.now()
                )

            insert_list.append(sql)
        insert_string = ','.join(map(str, insert_list))
        return insert_string
