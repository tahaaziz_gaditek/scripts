from spark import Spark
from config import configuration
from datetime import date as dd, datetime as dt, timedelta
from mixpanel import Mixpanel
from db import DB
from emailhandler import email_on_failure


class Engagement(Spark):
    def __init__(self, **params):
        Spark.__init__(self, **params)
        conf = configuration.get('mixpanel_production')
        self.mp = Mixpanel(conf.get('token'))

    # write users of new_client_profiling in user_engagement.billing_users where nc.first_paid_invoice_date  = startdate
    @email_on_failure
    def getBillingUsers(self, **params):
        """required: start_date , end_date"""
        if params and params.get('start_date') and params.get('offset') is not None:
            start_date = params.get('start_date')
            offset = params.get('offset')
            # end_date = (None, params.get('end_date'))[
            #    params.get('end_date') is not None]
        else:
            raise Exception("Invalid Number of Arguments were sent")

        query = """SELECT
            nc.first_paid_invoice_date AS first_invoice_date_paid,
            LOWER(TRIM(v.`username`)) AS username,
            LOWER(TRIM(nc.billing_cycle)) AS billing_cycle,
            LOWER(TRIM(substring_index(nc.`tags`,' ',1))) AS tags ,
            LOWER(TRIM(nc.`mixpanel_distinct_id`)) AS mixpanel_id,
            nc.`client_id` as client_id
            FROM
            new_client_profiling nc
            JOIN tblvpnusernames v
                ON nc.`client_id` = v.`clientid`
            WHERE nc.tags LIKE 'onbrd_%'
            AND nc.signup_date = nc.first_paid_invoice_date
            AND nc.first_paid_invoice_date  between '{0}' and date_add('{0}', INTERVAL {1} - 1 DAY) """.format(start_date, offset)

        print(query)
        df_billing = self.read_from_sql(
            db="billing_read_only", query=query)

        df_billing.createOrReplaceTempView("df_bl")
        df_billing.show()
        print("getBillingUsers compelted")
        conf = configuration.get('user_engagement')

        return "getBillingUsers executed"

    @email_on_failure
    def getWeekTime(self, **params):
        if params and params.get('ipd') and params.get('cycle') and params.get('offset') is not None:
            print("gettingweekTime")
            first_invoice_paid = dt.strptime(
                params.get('ipd'), '%Y-%m-%d').date()
            offset = params.get('offset')
            cycle = params.get('cycle')

            # these dates are set for networkdate
            if cycle > 0 and cycle <= 4:
                start_date = first_invoice_paid
                end_date = first_invoice_paid + timedelta(days=offset - 1)

                # update nl session time for users in  df_bl and append them in  user_time_spent
                print("executing  getSessionTime ")
                self.getSessionTime(start_date=start_date,
                                    end_date=end_date, offset=offset, cycle=cycle)

                print("executing  getNetworkMetrics ")
                self.getNetworkMetrics(
                    first_invoice_paid=first_invoice_paid, offset=offset, cycle=cycle)

            else:
                raise Exception("Invalid Week Tag was Sent")
        else:
            raise Exception("Invalid number of arguments were sent")

    @email_on_failure
    def getSessionTime(self, **params):
        if params and params.get('start_date') and params.get('end_date') and params.get('offset') and params.get('cycle') is not None:

            start_date = params.get('start_date')
            end_date = params.get('end_date')
            offset = params.get('offset')
            cycle = params.get('cycle')
        else:
            raise Exception("Invalid number of arguments were sent")

        query = """ select TRIM(LOWER(username)) as username, IFNULL(feature_value, 0) as time_spent , `date` as nldate from
                    churn_features.nl_total_session_time where `date` between '{0}' and '{1}'
        """.format(start_date, start_date+timedelta(days=offset + (7*cycle)-1))
        print("running nl")
        print(query)
        df_nl = self.spark.sql(query)
        df_nl.show()
        df_nl.createOrReplaceTempView("df_nl")

        query = "SELECT \
                    t1.first_invoice_date_paid, \
                    t1.username, \
                    t1.client_id, \
                    t1.billing_cycle, \
                    t1.tags, \
                    t1.mixpanel_id, \
                    sum(t2.time_spent)/60 as time_spent \
                FROM df_bl as t1  \
                LEFT JOIN df_nl as t2 \
                ON t1.username = t2.username \
                WHERE t2.nldate BETWEEN date_add(t1.first_invoice_date_paid, 7*({1} - 1) ) AND \
                date_add(t1.first_invoice_date_paid, (7 * {1}) - 1 ) \
                group by t1.first_invoice_date_paid, t1.username,t1.client_id," \
                " t1.billing_cycle, t1.tags, t1.mixpanel_id having sum(t2.time_spent) >0 and sum(t2.time_spent)/60 < 24*7*5".format(
            offset, cycle)
        # ORDER BY time_spent asc"

        print(query)
        df_time_spent = self.spark.sql(query)
        df_time_spent.show()
        df_time_spent.createOrReplaceTempView("bl_nl_users")
        print(" bl_nl_users compelted")

        query = "SELECT \
                    t1.first_invoice_date_paid, \
                    t1.client_id, \
                    t1.billing_cycle, \
                    t1.tags, \
                    t1.mixpanel_id, \
                    sum(t2.time_spent)/60 as time_spent \
                FROM df_bl as t1  \
                LEFT JOIN df_nl as t2 \
                ON t1.username = t2.username \
                WHERE t2.nldate BETWEEN date_add(t1.first_invoice_date_paid, 7*({1} - 1) ) AND \
                date_add(t1.first_invoice_date_paid, (7 * {1}) - 1 ) \
                group by t1.first_invoice_date_paid,t1.client_id," \
                " t1.billing_cycle, t1.tags, t1.mixpanel_id having sum(t2.time_spent) >0 and sum(t2.time_spent)/60 < 24*7*5".format(
            offset, cycle)
        # ORDER BY time_spent asc"

        print(query)
        df_time_spent = self.spark.sql(query)
        df_time_spent.show()
        df_time_spent.createOrReplaceTempView("bl_nl_client")
        print(" bl_nl_client compelted")
        return True

    @email_on_failure
    def getNetworkMetrics(self, first_invoice_paid, **params):
        query_df1 = """
                SELECT
                    tags,
                    count(distinct username) user_count,
                    count(distinct client_id) client_count,
                    SUM(time_spent) session_sum
                    FROM
                        bl_nl_users group by tags
        """
        df = self.spark.sql(query_df1)
        df.createOrReplaceTempView("df1")

        print(query_df1)
        # df2 deleted
        df.show()
        _df = df.collect()

        offset = params.get('offset')
        cycle = params.get('cycle')
        end_date = first_invoice_paid + timedelta(days=offset - 1)

        query_df3_2 = """select '{0}' as start_date, '{1}' as end_date, {2} as offset ,{3} as cycle,  df1.tags, df1.client_count,
            df1.session_sum, SUM(POW((bnu.time_spent - (df1.session_sum / df1.client_count)), 2)) as sq_x_minus_mean ,
            SQRT(SUM(POW((bnu.time_spent - (df1.session_sum /df1.client_count)), 2)) / df1.client_count) AS client_std,
            df1.session_sum /df1.client_count as mean
            FROM bl_nl_client as bnu
            left join df1
            on bnu.tags = df1.tags
            group by df1.tags, df1.client_count,df1.session_sum, df1.session_sum /df1.client_count""".format((first_invoice_paid), (end_date), offset, cycle)

        df_client = self.spark.sql(query_df3_2)
        df_client.createOrReplaceTempView("df3_2")
        df_client.show()

        _df = df_client.collect()

        sq_x_minus_mean = _df[0]['sq_x_minus_mean']

        conf = configuration.get('user_engagement')

        JDBC_CS_URL = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(conf.get(
            'host'), conf.get('port'), conf.get('database'), conf.get('user'), conf.get('password'))

        write_status = df_client.write.format('jdbc') \
            .options(
            url=JDBC_CS_URL, dbtable='client_time_spent_stats', driver="com.mysql.jdbc.Driver") \
            .mode('append') \
            .save()

        print("write_status for stats" + str(write_status))

        query_df4_client = """ select t1.* , (t1.time_spent -  (df3_2.session_sum /df3_2.client_count)) / df3_2.client_std as z_score  from bl_nl_client as t1
        left join df3_2
        on t1.tags=df3_2.tags"""  # .format(session_sum, user_count, user_std)

        df = self.spark.sql(query_df4_client)
        df.createOrReplaceTempView("df4_2")
        df.show()

        query_df6 = """ select df4_2.* , {0} as cycle,
                    case when df4_2.time_spent <=0 then "dead"
                        when df4_2.z_score <= 0 then "casual"
                        when df4_2.z_score > 0  and  df4_2.z_score <= 0 + 2 then "core"
                        when df4_2.z_score > 0 + 2  then "power"
                        else 'dead' end as tag
                    from df4_2
                    """.format(cycle)
        df_tags = self.spark.sql(query_df6)

        df.show()

        conf = configuration.get('user_engagement')
        JDBC_CS_URL = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(conf.get(
            'host'), conf.get('port'), conf.get('database'), conf.get('user'), conf.get('password'))

        write_status = df_tags.write.format('jdbc') \
            .options(
            url=JDBC_CS_URL, dbtable='client_time_spent_tags', driver="com.mysql.jdbc.Driver") \
            .mode('append') \
            .save()

        print("write_status for tags"+str(write_status))

        return True

    @email_on_failure
    def applyTags(self, **params):
        if params and params.get('start_date') and params.get('start_date_train') and params.get('offset') is not None \
                and params.get('cycle') is not None:
            start_date = params.get('start_date')
            offset = params.get('offset')
            cycle = params.get('cycle')
            start_date_train = params.get('start_date_train')
            # end_date = (None, params.get('end_date'))[
            #    params.get('end_date') is not None]
        else:
            raise Exception("Invalid Number of Arguments were sent")

        query = """SELECT
            LOWER(TRIM(nc.first_paid_invoice_date)) AS first_invoice_date_paid,
            LOWER(TRIM(v.`username`)) AS username,
            LOWER(TRIM(nc.billing_cycle)) AS billing_cycle,
            LOWER(TRIM(substring_index(nc.`tags`,' ',1))) AS tags ,
            LOWER(TRIM(nc.`mixpanel_distinct_id`)) AS mixpanel_id ,
            nc.`client_id` as client_id
            FROM
            new_client_profiling nc
            JOIN tblvpnusernames v
                ON nc.`client_id` = v.`clientid`
            WHERE nc.tags LIKE 'onbrd_%'
            AND signup_date = first_paid_invoice_date
            AND nc.first_paid_invoice_date  = '{0}' """.format(start_date)

        df_billing = self.read_from_sql(
            db="billing_read_only", query=query)

        df_billing.createOrReplaceTempView("df_bl_test")
        df_billing.show(100)
        print("getBillingUsers test compelted")

        start_date = dt.strptime(params.get(
            'start_date'), '%Y-%m-%d').date()

        query = """ select TRIM(LOWER(username)) as username, IFNULL(feature_value, 0) as time_spent , `date` as nldate from
                    churn_features.nl_total_session_time where `date` between '{0}' and '{1}'
        """.format(start_date, start_date + timedelta(days=(7 * cycle) - 1))

        print("running nl")
        print(query)
        df_nl = self.spark.sql(query)
        df_nl.show()
        df_nl.createOrReplaceTempView("df_nl_test")

        query = "SELECT \
                    t1.first_invoice_date_paid, \
                    t1.billing_cycle, \
                    t1.tags, \
                    t1.mixpanel_id, \
                    IFNULL(sum(t2.time_spent)/60, 0)  as time_spent," \
                    "t1.`client_id` as client_id \
                FROM df_bl_test as t1  \
                LEFT JOIN df_nl_test as t2 \
                ON t1.username = t2.username \
                and t2.nldate BETWEEN date_add(t1.first_invoice_date_paid, 7*({1} - 1) ) AND \
                date_add(t1.first_invoice_date_paid, (7 * {1}) - 1 )\
                group by t1.first_invoice_date_paid, t1.client_id," \
                " t1.billing_cycle, t1.tags, t1.mixpanel_id ".format(
                    offset, cycle)
        # ORDER BY time_spent asc"

        print(query)
        df_time_spent = self.spark.sql(query)
        df_time_spent.show(100)
        df_time_spent.createOrReplaceTempView("bl_nl_users_test")
        print("bl_nl_users_test compelted")

        query = """SELECT *  FROM
            client_time_spent_stats
            WHERE  start_date= '{0}' and offset = {1} and cycle ={2}""".format(start_date_train, offset, cycle)

        print(query)

        # df_thresholds = self.read_from_sql(db="user_engagement", query=query)
        conf = configuration.get('user_engagement')

        df_thresholds = self.spark.read \
            .format("jdbc") \
            .options(
                url="jdbc:mysql://{0}:{2}/{1}".format(
                    conf.get('host'), 'user_engagement', conf.get('port')),
                driver='com.mysql.jdbc.Driver',
                dbtable="""({0}) as t1""".format(query),
                user=conf.get('user'),
                password=conf.get('password')).load()

        df_thresholds.show(100)
        df_thresholds.createOrReplaceTempView("thresholds")

        query_df4 = """ select t1.* , (t1.time_spent -  (thresholds.session_sum /thresholds.client_count)) / thresholds.client_std as z_score
                from   bl_nl_users_test as t1
                left join thresholds
                on t1.tags=thresholds.tags"""  # .format(session_sum, user_count, user_std)

        df = self.spark.sql(query_df4)

        df.createOrReplaceTempView("df4")
        df.show()

        query_df5 = """ select avg(z_score), sum(z_score) as avg_z_score from df4 """

        df = self.spark.sql(query_df5)
        df.createOrReplaceTempView("df5")
        df.show()

        query_df6 = """ select df4.* , {0} as cycle, '{1}' as start_date_train, {2} as offset,
                            case when df4.time_spent <=0 then "dead"
                                when df4.time_spent >= 24*5*7 then "super power"
                                when df4.z_score <= 0 then "casual"
                                when df4.z_score > 0  and  df4.z_score <= 0 + 2 then "core"

                                else 'power' end as tag
                            from df4
                            """.format(cycle, start_date_train, offset)
        # when df4.z_score > 0 + 2  then "power"
        df_tags = self.spark.sql(query_df6)

        df.show(100)

        conf = configuration.get('user_engagement')

        JDBC_CS_URL = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(conf.get(
            'host'), conf.get('port'), conf.get('database'), conf.get('user'), conf.get('password'))

        write_status = df_tags.write.format('jdbc') \
            .options(
            url=JDBC_CS_URL, dbtable='client_time_spent_tags_test', driver="com.mysql.jdbc.Driver") \
            .mode('append') \
            .save()

        return True

    @email_on_failure
    def sendTagsToMixpanel(self, **params):
        if params and params.get('start_date') is not None:
            start_date = params.get('start_date')
            cycle = (1, params.get('cycle'))[params.get('cycle') is not None]
        else:
            raise Exception("Invalid Number of Arguments were Sent in SendTagsMethodToMixpanel method")

        odb = DB('user_engagement')

        query = """SELECT 
                    SUBSTRING_INDEX(t1.tags, "_", - 1) AS purpose,
                    t1.tag,
                    TRIM(t1.mixpanel_id) AS mixpanel_id,
                    t1.billing_cycle,
                    cycle AS `week`,
                    IFNULL(
                        (SELECT 
                        tag 
                        FROM
                        `client_time_spent_tags_test` 
                        WHERE cycle = t1.cycle - 1 
                        AND first_invoice_date_paid = t1.first_invoice_date_paid 
                        AND client_id = t1.client_id),
                        "N/A"
                    ) AS previous_tag 
                FROM
                    client_time_spent_tags_test  as t1
                WHERE first_invoice_date_paid = '{}' 
                AND cycle = {}
                """.format(start_date, cycle)
        
        mixpanel_data = odb.execute(query)
        event_properties = dict()

        for data in mixpanel_data:
            event_properties['tag'] = data.get('tag')
            event_properties['purpose'] = data.get('purpose')
            event_properties['week'] = data.get('week')
            event_properties['previous_tag'] = data.get('previous_tag')
            self.mp.track(data.get('mixpanel_id'), "Engagement_Tag" , event_properties)
