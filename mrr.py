from db import DB
from datetime import date as dd, datetime as dt, timedelta
from emailhandler import InitializeLogging
import logging


class MRR():
    def __init__(self, **params):
        self.appName = ("MRR", params.get('app'))[
            params.get('app') is not None]
        InitializeLogging(app=self.appName, debug=True)
        self.logger = logging.getLogger("MRR Reporting Script")
        self.logger.setLevel(logging.DEBUG)

    def extractHostingDataFromBilling(self, **params):
        """required: start_date , end_date"""
        try:
            if params and params.get('start_date') is not None:

                start_date = params.get('start_date')
                offset = (0, params.get('offset'))[
                    params.get('offset') is not None]
                start_date = dt.strptime(
                    start_date, '%Y-%m-%d').date()

                end_date = start_date + timedelta(days=offset)
                dates = ['{:%Y-%m-%d}'.format(start_date+timedelta(days=x))
                         for x in range(0, offset+1)]
            else:
                raise Exception("Invalid Number of Arguments were sent")

            for date in dates:

                hosting_raw_data = dict()
                insert_list = list()

                self.logger.info(
                    "Preparing Hosting Data For Date ".format(date))

                query = """
                    SELECT
                    DATE(i.`datepaid`) AS invoice_date_paid,
                    c.id AS client_id,
                    h.id AS hosting_id,
                    i.id AS invoice_id,
                    CASE
                    WHEN h.billingcycle = 'semi-annually' 
                    THEN 'semiannually' 
                    ELSE LOWER(TRIM(h.billingcycle)) 
                    END AS billing_cycle,
                    LOWER(TRIM(it.type)) AS `type`,
                    LOWER(TRIM(g.name)) AS product_group_name,
                    LOWER(TRIM(i.`paymentmethod`)) AS payment_method,
                    it.amount,
                    IFNULL(
                        ROUND(
                        IFNULL(
                            JSON_EXTRACT_C (`value`, 'hosting_id'),
                            ''
                        )
                        ),
                        -1
                    ) AS old_hosting_id,
                    CASE
                        WHEN cfv.relid = i.id
                        THEN 1
                        ELSE 0
                    END AS is_upgraded
                    FROM
                    tblclients c
                    JOIN tblinvoices i
                        ON c.`id` = i.`userid`
                    JOIN tblinvoiceitems it
                        ON it.`invoiceid` = i.`id`
                    JOIN tblhosting h
                        ON h.id = it.`relid`
                    JOIN tblaccounts a
                        ON a.`invoiceid` = i.`id`
                    LEFT JOIN tblcustomfieldsvalues cfv
                        ON cfv.relid = i.id
                        AND cfv.value <> ''
                        AND cfv.fieldid = 28
                    JOIN tblproducts p
                        ON p.id = h.packageid
                    JOIN tblproductgroups g
                        ON p.gid = g.id
                    WHERE DATE(i.`datepaid`) = "{0}"
                    AND i.`status` = 'Paid'
                    AND it.`type` = 'Hosting'
                    AND h.`domainstatus` IN ('Active', 'Cancelled')
                    AND h.billingcycle NOT IN ('Free Account', 'One Time')
                    AND it.relid > 0
                    AND it.amount > 0
                        """.format(date)

                odb = DB('billing_read_only')

                hosting_raw_data = odb.execute(query)

                if (len(hosting_raw_data) > 0):
                    for record in hosting_raw_data:
                        sql = """( '{0}', {1}, {2}, {3}, '{4}', '{5}', '{6}', '{7}', {8}, {9}, {10}, '{11}' )"""\
                            .format(
                                record.get('invoice_date_paid'),
                                record.get('client_id'),
                                record.get('hosting_id'),
                                record.get('invoice_id'),
                                record.get('type'),
                                record.get('product_group_name'),
                                record.get('billing_cycle'),
                                record.get('payment_method'),
                                record.get('amount'),
                                record.get('old_hosting_id'),
                                record.get('is_upgraded'),
                                dt.now()
                            )

                        insert_list.append(sql)

                    insert_string = ','.join(map(str, insert_list))

                    query = """INSERT INTO mrr_hosting_paid_raw(
                                invoice_date_paid,
                                client_id,
                                hosting_id,
                                invoice_id,
                                type,
                                product_group_name,
                                billing_cycle,
                                payment_method,
                                amount,
                                old_hosting_id,
                                is_upgraded,
                                added_on
                                )
                            VALUES {} """.format(insert_string)

                    odb.close()
                    odb = DB('mrr_hosting')
                    inserted_rows = odb.execute(query)

                    self.logger.info(
                        "{} Rows Inserted in mrr_hosting_paid_raw".format(inserted_rows))

                else:
                    raise Exception("No Records were found in Database")

            self.logger.info(
                'Running Mrr_Prepare_Data_For_Hosting for Dates {} and {}'.format(start_date, end_date))

            result = odb.callSp(
                'mrr_prepare_hosting_data', '{}'.format(start_date), offset)

            odb.close()

            self.logger.info("Hosting Data for Dates {} and {} has been prepared Successfully!".format(
                start_date, ))

            return result
        except Exception as e:
            self.logger.error(str(e))

    def hostingDateDistribution(self, **params):
        try:
            if params and params.get('start_date') is not None:

                start_date = params.get('start_date')

                offset = (0, params.get('offset'))[
                    params.get('offset') is not None]

                start_date = dt.strptime(
                    start_date, '%Y-%m-%d').date()

                dates = ['{:%Y-%m-%d}'.format(start_date+timedelta(days=x))
                         for x in range(0, offset+1)]
            else:
                raise Exception("Invalid Number of Arguments were sent")

            odb = DB('mrr_hosting')

            for date in dates:

                self.logger.info(
                    'Preparing hosting distribution for date {}'.format(date))

                query = """SELECT
                    m.`adjusted_invoice_date_paid` AS mrr_date,
                    m.`hosting_id` AS id,
                    m.`billing_cycle` AS billing_cycle ,
                    m.`amount`  AS amount
                    FROM
                    `mrr_hosting_paid_data_v2` m
                    WHERE m.`invoice_date_paid` = '{0}'"""\
                    .format(date)

                hosting_data = odb.execute(query)

                if hosting_data is not None:

                    insert_string = self.prepareInsertString(data=hosting_data)

                    query = """INSERT IGNORE INTO mrr_hosting_calculation_dates(date_paid, hosting_id, monthly_amount, is_booked, booked_date, added_on)
                            VALUES {}""".format(insert_string)

                    result = odb.execute(query)
                    self.logger.info('{0}'.format(
                        result))
                    self.logger.info(
                        'Hosting Date Distribution data has been prepared for date {} Successfully!'.format(date))
                else:
                    raise Exception("No Records were found in Database")
        except Exception as e:
            self.logger.error(str(e))

    def extractAddonDataFromBilling(self, **params):
        try:
            if params and params.get('start_date') is not None:

                start_date = params.get('start_date')

                offset = (0, params.get('offset'))[
                    params.get('offset') is not None]

                start_date = dt.strptime(
                    start_date, '%Y-%m-%d').date()

                end_date = start_date + timedelta(days=offset)
                dates = ['{:%Y-%m-%d}'.format(start_date+timedelta(days=x))
                         for x in range(0, offset+1)]
            else:
                raise Exception("Invalid Number of Arguments were sent")

            for date in dates:
                addon_raw_data = dict()
                insert_list = list()

                self.logger.info(
                    "Preparing Addon Data For Date {0} ".format(date))

                query = """
                    SELECT
                        DATE(i.`datepaid`) AS invoice_date_paid,
                        c.id AS client_id,
                        h.id AS hosting_id,
                        ah.`id` AS addon_id,
                        i.id AS invoice_id,
                        LOWER(TRIM(ah.`billingcycle`)) AS addon_cycle,
                        CASE
                        WHEN h.billingcycle = 'semi-annually' 
                        THEN 'semiannually' 
                        ELSE LOWER(TRIM(h.billingcycle)) 
                        END AS hosting_billing_cycle,
                        LOWER(TRIM(it.type)) AS `type`,
                        SUBSTRING_INDEX(LOWER(TRIM(it.`description`)), '(', 1) AS item,
                        LOWER(TRIM(i.`paymentmethod`)) AS payment_method,
                        it.`amount`
                    FROM
                        tblclients c
                    JOIN tblinvoices i
                            ON c.`id` = i.`userid`
                    LEFT JOIN tblinvoiceitems it
                            ON it.`invoiceid` = i.`id`
                    LEFT JOIN tblhostingaddons ah
                            ON ah.id = it.`relid`
                    LEFT JOIN tblhosting h
                            ON ah.`hostingid` = h.`id`
                    LEFT JOIN tblaccounts a
                            ON a.`invoiceid` = i.`id`
                    WHERE DATE(i.`datepaid`) = '{}'
                        AND i.`status` = 'Paid'
                        AND it.`type` = 'Addon'
                        AND ah.status IN ('Active', 'Cancelled')
                        AND h.`domainstatus` IN ('Active', 'Cancelled')
                        AND ah.`billingcycle` NOT IN ('Free Account', 'One Time')
                        AND h.`billingcycle` NOT IN ('One Time')
                        AND it.relid > 0
                        AND it.`amount` > 0
                        """.format(date)

                odb = DB('billing_read_only')

                addon_raw_data = odb.execute(query)

                if len(addon_raw_data) > 0:
                    for record in addon_raw_data:
                        sql = """( '{0}', {1}, {2}, {3}, {4}, '{5}', '{6}', '{7}', '{8}', '{9}', {10}, '{11}' )"""\
                            .format(
                                record.get('invoice_date_paid'),
                                record.get('client_id'),
                                record.get('hosting_id'),
                                record.get('addon_id'),
                                record.get('invoice_id'),
                                record.get('addon_cycle'),
                                record.get('hosting_billing_cycle'),
                                record.get('type'),
                                record.get('item'),
                                record.get('payment_method'),
                                record.get('amount'),
                                dt.now()
                            )

                        insert_list.append(sql)

                    insert_string = ','.join(map(str, insert_list))

                    query = """INSERT INTO mrr_addon_paid_raw(
                        invoice_date_paid,
                        client_id,
                        hosting_id,
                        addon_id,
                        invoice_id,
                        addon_cycle,
                        hosting_billing_cycle,
                        type,
                        item,
                        payment_method,
                        amount,
                        added_on
                        )
                    VALUES {} """.format(insert_string)

                    odb.close()
                    odb = DB('mrr_addon')
                    result = odb.execute(query)
                    
                    self.logger.info('{} Rows have been inserted in Mrr Addon Paid Raw'.format(result))

                    self.logger.info('Running Mrr Prepare Data For Addon For Dates {} and {}'.format(
                        start_date, end_date))

                    result = odb.callSp('mrr_prepare_addon_data', '{}'.format(start_date), offset)
                    
                    self.logger.info("Addon Data For Dates {} And {} Has Been Prepared Successfully!".format(
                        start_date, end_date))
                else:
                    raise Exception("No Records were found in Database")
        except Exception as e:
            self.logger.error(str(e))
        return result

    def addonDateDistribution(self, **params):
        try:
            if params and params.get('start_date') is not None:

                start_date = params.get('start_date')

                offset = (0, params.get('offset'))[
                    params.get('offset') is not None]

                start_date = dt.strptime(
                    start_date, '%Y-%m-%d').date()

                dates = ['{:%Y-%m-%d}'.format(start_date+timedelta(days=x))
                         for x in range(0, offset+1)]
            else:
                raise Exception("Invalid Number Of Arguments Were Sent")

            odb = DB('mrr_addon')

            for date in dates:

                self.logger.info(
                    'Preparing addon distribution for date {}'.format(date))

                query = """SELECT 
                        m.`adjusted_invoice_date_paid` AS mrr_date,
                        m.`addon_id` AS id,
                        m.`addon_cycle` AS billing_cycle,
                        m.`amount` AS amount
                        FROM
                        mrr_addon_paid_data_v2 m 
                        WHERE m.invoice_date_paid = '{}' 
                        GROUP BY 1,2,3""".format(date)

                addon_data = odb.execute(query)

                if addon_data is not None:

                    insert_string = self.prepareInsertString(data=addon_data)

                    query = """INSERT IGNORE INTO mrr_calculation_dates(date_paid, addon_id, monthly_amount, is_booked, booked_date, added_on)
                            VALUES {}""".format(insert_string)

                    result = odb.execute(query) 

                    self.logger.info('{0}'.format(result))
                    self.logger.info('Addon Date Distribution Data Has Been Prepared For Date {} Successfully!'.format(date))
                else:
                    raise Exception("No Records Were Found In Database")
        except Exception as e:
            self.logger.error(str(e))

    def hostingChurnDateDistribution(self, **params):
        try:
            if params and params.get('start_date') is not None:

                start_date = params.get('start_date')

                offset = (0, params.get('offset'))[
                    params.get('offset') is not None]

                start_date = dt.strptime(
                    start_date, '%Y-%m-%d').date()

                dates = ['{:%Y-%m-%d}'.format(start_date+timedelta(days=x))
                         for x in range(0, offset+1)]
            else:
                raise Exception("Invalid Number Of Arguments Were Sent")

            odb = DB('mrr_hosting')

            for date in dates:

                self.logger.info('Preparing Hosting Distribution For Date {}'.format(date))

                query = """SELECT 
                        m.`churn_date` AS mrr_date,
                        m.`hosting_id` AS id,
                        m.`billing_cycle` AS billing_cycle,
                        m.`amount`  AS amount 
                        FROM
                        `mrr_churn_data` m 
                        WHERE m.churn_date = '{}' 
                        GROUP BY 1,
                        2,
                        3 """.format(date)

                hosting_data = odb.execute(query)

                if hosting_data is not None:

                    insert_string = self.prepareInsertString(data=hosting_data)

                    query = """INSERT IGNORE INTO mrr_hosting_calculation_dates(date_paid, hosting_id, monthly_amount, is_booked, booked_date, added_on)
                            VALUES {}""".format(insert_string)

                    result = odb.execute(query)
                    self.logger.info('{0}'.format(
                        result))
                    self.logger.info(
                        'Hosting Churn Date Distribution Data Has Been Prepared For Date {} Successfully!'.format(date))
                else:
                    raise Exception("No Records Were Found In Database")
        except Exception as e:
            self.logger.error(str(e))

    def addonChurnDateDistribution(self, **params):
        try:
            if params and params.get('start_date') is not None:

                start_date = params.get('start_date')

                offset = (0, params.get('offset'))[
                    params.get('offset') is not None]

                start_date = dt.strptime(
                    start_date, '%Y-%m-%d').date()

                dates = ['{:%Y-%m-%d}'.format(start_date+timedelta(days=x))
                         for x in range(0, offset+1)]
            else:
                raise Exception("Invalid Number Of Arguments Were Sent")

            odb = DB('mrr_addon')

            for date in dates:

                self.logger.info(
                    'Preparing Addon Churn Date Distribution For Date {}'.format(date))

                query = """
                    SELECT 
                        m.`churn_date` AS mrr_date,
                        m.`addon_id` AS id,
                        m.`addon_cycle` AS billing_cycle,
                        m.`amount` * -1 AS amount 
                    FROM
                        mrr_addon_churn_data m 
                    WHERE m.churn_date = '{}' 
                    GROUP BY 1,
                        2,
                        3""".format(date)

                addon_data = odb.execute(query)

                if addon_data is not None:

                    insert_string = self.prepareInsertString(data=addon_data)

                    query = """INSERT IGNORE INTO mrr_calculation_dates(date_paid, addon_id, monthly_amount, is_booked, booked_date, added_on)
                            VALUES {}""".format(insert_string)

                    result = odb.execute(query) 

                    self.logger.info('{0}'.format(result))
                    self.logger.info('Addon Churn Date Distribution Data Has Been Prepared For Date {} Successfully!'.format(date))
                else:
                    raise Exception("No Records Were Found In Database")
        except Exception as e:
            self.logger.error(str(e))

    def prepareInsertString(self, **params):
        try:
            if params is not None and params.get('data') is not None:
                data = params.get('data')

            elif len(params.get('data')) == 0:
                raise Exception('Data Dictionary Is Empty')

            else:
                raise Exception('Invalid Number of Arguments Sent')

            insert_list = list()

            for record in data:
                mrr_date = booked_date = record.get('mrr_date')
                _id = record.get('id')
                billing_cycle = record.get('billing_cycle')
                mrr_amount = record.get('amount')

                if billing_cycle == 'monthly':
                    is_booked = 0
                    monthly_amount = mrr_amount

                    sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                        .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())

                    insert_list.append(sqlstring)

                if billing_cycle == 'annually':
                    cycle = 12
                    is_booked = 0
                    monthly_amount = mrr_amount / cycle

                    sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                        .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())

                    insert_list.append(sqlstring)

                    for i in range(0, cycle-1):
                        is_booked = 1
                        mrr_date = mrr_date + timedelta(days=30)
                        sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                            .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())
                        insert_list.append(sqlstring)

                if billing_cycle == 'semi-annually' or billing_cycle == 'semi-annually':
                    cycle = 6
                    is_booked = 0
                    monthly_amount = mrr_amount / cycle

                    sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                        .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())

                    insert_list.append(sqlstring)

                    for i in range(0, cycle-1):
                        is_booked = 1
                        mrr_date = mrr_date + timedelta(days=30)
                        sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                            .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())
                        insert_list.append(sqlstring)

                if billing_cycle == 'quarterly':
                    cycle = 3
                    is_booked = 0
                    monthly_amount = mrr_amount / cycle

                    sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                        .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())

                    insert_list.append(sqlstring)

                    for i in range(0, cycle-1):
                        is_booked = 1
                        mrr_date = mrr_date + timedelta(days=30)
                        sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                            .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())
                        insert_list.append(sqlstring)

                if billing_cycle == 'triennially':
                    cycle = 36
                    is_booked = 0
                    monthly_amount = mrr_amount / cycle

                    sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                        .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())

                    insert_list.append(sqlstring)

                    for i in range(0, cycle-1):
                        is_booked = 1
                        mrr_date = mrr_date + timedelta(days=30)
                        sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                            .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())
                        insert_list.append(sqlstring)

                if billing_cycle == 'biennially':
                    cycle = 24
                    is_booked = 0
                    monthly_amount = mrr_amount / cycle

                    sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                        .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())

                    insert_list.append(sqlstring)

                    for i in range(0, cycle-1):
                        is_booked = 1
                        mrr_date = mrr_date + timedelta(days=30)
                        sqlstring = """( '{0}' , {1} , {2} , {3} , '{4}', '{5}' ) """\
                            .format(mrr_date, _id, monthly_amount, is_booked, booked_date, dt.now())
                        insert_list.append(sqlstring)

            insert_string = ','.join(map(str, insert_list))
        except Exception as e:
            self.logger.error(str(e))
            raise
        return insert_string

    def prepareMRRReport(self, **params):
        
        start_date = ('2017-01-01', params.get('start_date')
                      )[params.get('start_date') is not None]
        
        odb = DB('mrr_hosting')
        
        result = odb.callSp('mrr_report_release', '{}'.format(start_date))

    def prepareClientChurnReport(self, **params):
        try:
            if params and params.get('start_date') is not None:
                start_date = params.get('start_date')
                else:
                    raise Exception("Invalid Number Of Arguments Were Sent")
                odb = DB('mrr_hosting')
                ow1db.callSp('mrr_active_clients', '{}'.format(start_date))
        except Exception  as e:
            self.logger.error(str(e))