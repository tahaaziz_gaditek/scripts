from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
import config as db_config

conf  = SparkConf().setAppName("MRR Hosting Distribution")
sc    = SparkContext(conf=conf)
spark = SparkSession.builder.config("hive.metastore.uris",
"thrift://hmaster:9084").enableHiveSupport().getOrCreate()

db = db_config.configuration

date = '2018-01-01'

query = """SELECT
  adjusted_invoice_date_paid AS mrr_date,
  hosting_id,
  billing_cycle,
  amount
FROM
  `mrr_hosting_paid_data_v2`
WHERE adjusted_invoice_date_paid = {};
 """.format(date)

print(db['mrr_hosting'])

df_mrr = spark.read.format("jdbc").options(
    url      = "jdbc:mysql://{0}:3306/{1}".format(db['mrr_hosting'].get('hostname'), db['mrr_hosting'.get('db')]),
    driver   = "com.mysql.jdbc.Driver",
    dbtable  = """({}) as t1""".format(query) ,
    user     = db['mrr_hosting'].get('user'),
    password = db['mrr_hosting'].get('password')).load()

print (df_mrr)

