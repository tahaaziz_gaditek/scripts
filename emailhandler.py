import sys
import logging
import logging.handlers
import smtplib

from logging import StreamHandler
from config import recipients, sender
from functools import wraps

PYTHON_VERSION = sys.version_info[0]

if PYTHON_VERSION > 2:
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
else:
    from email.MIMEMultipart import MIMEMultipart
    from email.MIMEText import MIMEText


class EmailHandler(StreamHandler):

    def __init__(self, **params):

        StreamHandler.__init__(self)

        self.subject = params.get('app') if params.get('app') is not None else " "
        self.recipients = recipients
        self.sender = sender
        self.server = self._get_server()
        self.envelope = MIMEMultipart()

    def emit(self, record):

        msg = self.format(record)
        recipients_string = ", ".join(self.recipients)
        self.envelope['From'] = self.sender.get('DLNotify')
        self.envelope['To'] = recipients_string
        self.envelope['Subject'] = "[FAILURE] {} JOB HAS BEEN FAILED".format(
            self.subject)
        email_msg = MIMEText(msg, "plain")
        self.envelope.attach(email_msg)

        status = self.server.sendmail(self.sender.get('DLNotify'),  self.recipients,
                             self.envelope.as_string())

        print("email sent. Status {0}".format(status))

    def _get_server(self):

        server = smtplib.SMTP()
        server.connect('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(self.sender.get('DLNotify'),
                     self.sender.get('password'))

        return server


class InitializeLogging:

    def __init__(self, **params):

        print("Initializing logging")
        self.debug = params.get('debug') if params.get('debug') is not None else True

        self.logger = logging.getLogger()
        self.formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        self.fh = logging.FileHandler('script.log')
        self.fh.setLevel(logging.DEBUG)
        self.fh.setFormatter(self.formatter)

        self.sh = logging.StreamHandler()
        self.sh.setLevel(logging.ERROR)
        self.sh.setFormatter(self.formatter)

        self.logger.addHandler(self.fh)
        self.logger.addHandler(self.sh)
        self.logger.setLevel(logging.DEBUG)

        if not self.debug:
            self.eh = EmailHandler(**params)
            self.eh.setLevel(logging.ERROR)
            self.eh.setFormatter(self.formatter)
            self.logger.addHandler(self.eh)

    def get_logger(self):
        return self.logger

    def set_debug(self, is_debug):
        assert isinstance(is_debug, bool), "pass in a boolean value"
        self.debug = is_debug


log = InitializeLogging(debug=False)


def email_on_failure(some_func):

    @wraps(some_func)
    def with_logging(*args, **kwargs):
        try:
            return some_func(*args, **kwargs)
        except Exception as e:
            error_msg = some_func.__name__ + "Failed To Run " + str(e)
            log.get_logger().error(error_msg)

    return with_logging
