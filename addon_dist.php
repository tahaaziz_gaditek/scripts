<?php

ini_set('memory_limit', '8024M');
date_default_timezone_set('Africa/Lagos');

#(invoice_date_paid as mrr_date, addon_id, amount, billing_cycle)
#nohup php addon_dist.php  >/dev/null 2>&1 &
#91.121.47.223

$array = array(
'triennially'  => '2014-01-10',
'annually'     => '2015-12-31',
'monthly'      => '2016-11-25',
'quarterly'    => '2016-09-26',
'semiannually' => '2016-06-28',
'biennially'   => '2015-01-05'
);

$current_date = '2014-06-04';
$start_date   = date_create($current_date);
$end_date     = date_create('2018-10-31');
$diff         = date_diff($end_date, $start_date);
$days         = $diff->days;

for($i=0; $i<=$days; $i++) {
            $dates[]       =  $current_date; 
            $current_date = date('Y-m-d', strtotime('+1 day', strtotime($current_date)));
}
print_r($dates);
exit();
foreach($dates as $key => $date) 
{
    $db = connect('mrr_addon_reporting');

    
    $query =  " SELECT 
                  m1.`adjusted_invoice_date_paid` AS mrr_date,
                  m1.`addon_id` AS addon_id,
                  m1.`addon_cycle` AS billing_cycle, 
                  m1.`amount` AS amount
                FROM
                  mrr_addon_paid_data_v2 m1 
                WHERE m1.`adjusted_invoice_date_paid` =  '".$date."';
                 ";
    
    $data  = execute_query($query);
    
    storeMonthlyDates($data);
    
}

function storeMonthlyDates($data = [])
{

    $mrr_data = [];
    $sql      = [];

    foreach ($data as $key => $value) {
        
        $mrr_data['addon_id']  = $value['addon_id'];
        $mrr_data['booked_date'] = $value['mrr_date'];
        $mrr_data['is_booked']   = 0;

        if (trim(strtolower($value['billing_cycle'])) == 'monthly') {
                
                $mrr_data['monthly_amount'] = $value['amount'];
                $date                       = $value['mrr_date'];
                $sql[] = '( 
                    "' .$date.'"  ,
                    '.$mrr_data['addon_id'].' ,
                    '.$mrr_data['monthly_amount'].' , 
                    "'.$mrr_data['booked_date'].'"  ,
                    '.$mrr_data['is_booked'].'
                )';
        }

        if (trim(strtolower($value['billing_cycle'])) == 'semi-annually' || trim(strtolower($value['billing_cycle'])) == 'semiannually') {
        
            $mrr_data['monthly_amount'] = $value['amount'] / 6;
            $mrr_data['cycles']         = 6;
            $date                       = $value['mrr_date'];
            for ($i = 0; $i <  $mrr_data['cycles']; $i++) {
                    $sql[] = '( 
                            "' .$date.'"                    ,
                            ' .$mrr_data['addon_id']. '   ,
                            '.$mrr_data['monthly_amount'].' ,
                            "'.$mrr_data['booked_date'].'"  ,
                            '.$mrr_data['is_booked'].'                
                    )';

                    $date = date('Y-m-d', strtotime('+30 day', strtotime($date)));
                    $mrr_data['is_booked'] = 1;
            }
            
        }

        if (trim(strtolower($value['billing_cycle'])) == 'annually') {
            $mrr_data['monthly_amount'] = $value['amount'] / 12;
            $mrr_data['cycles']         = 12;
            $date                       = $value['mrr_date'];
            for ($i=0; $i <  $mrr_data['cycles']; $i++) {
                    $sql[] = '( 
                            "' .$date.'"                    ,
                            ' .$mrr_data['addon_id']. '   ,
                            '.$mrr_data['monthly_amount'].' ,
                            "'.$mrr_data['booked_date'].'"  ,
                            '.$mrr_data['is_booked'].'                
                    )';

                    $date = date('Y-m-d', strtotime('+30 day', strtotime($date)));
                    $mrr_data['is_booked'] = 1;
            }
        }

        if (trim(strtolower($value['billing_cycle'])) == 'quarterly') {
            $mrr_data['monthly_amount'] = $value['amount'] / 3;
            $mrr_data['cycles']         = 3;
            $date                       = $value['mrr_date'];
            for ($i=0; $i <  $mrr_data['cycles']; $i++) {
                  $sql[] = '( 
                            "' .$date.'"                    ,
                            ' .$mrr_data['addon_id']. '   ,
                            '.$mrr_data['monthly_amount'].' ,
                            "'.$mrr_data['booked_date'].'"  ,
                            '.$mrr_data['is_booked'].'                
                    )';

                    $date = date('Y-m-d', strtotime('+30 day', strtotime($date)));
                    $mrr_data['is_booked'] = 1;
            }
        }

        if (trim(strtolower($value['billing_cycle'])) == 'biennially') {
            $mrr_data['monthly_amount'] = $value['amount'] / 24;
            $mrr_data['cycles']         = 24;
            $date                       = $value['mrr_date'];
            for ($i=0; $i <  $mrr_data['cycles']; $i++) {
                  $sql[] = '( 
                            "' .$date.'"                    ,
                            ' .$mrr_data['addon_id']. '   ,
                            '.$mrr_data['monthly_amount'].' ,
                            "'.$mrr_data['booked_date'].'"  ,
                            '.$mrr_data['is_booked'].'                
                    )';

                    $date = date('Y-m-d', strtotime('+30 day', strtotime($date)));
                    $mrr_data['is_booked'] = 1;
            }
        }

        if (trim(strtolower($value['billing_cycle'])) == '5year') {
            $mrr_data['monthly_amount'] = $value['amount'] / 60;
            $mrr_data['cycles']         = 60;
            $date                       = $value['mrr_date'];
            for ($i=0; $i <  $mrr_data['cycles']; $i++) {
                  $sql[] = '( 
                            "' .$date.'"                    ,
                            ' .$mrr_data['addon_id']. '   ,
                            '.$mrr_data['monthly_amount'].' ,
                            "'.$mrr_data['booked_date'].'"  ,
                            '.$mrr_data['is_booked'].'                
                    )';

                    $date = date('Y-m-d', strtotime('+30 day', strtotime($date)));
                    $mrr_data['is_booked'] = 1;
            }
        }

        if (trim(strtolower($value['billing_cycle'])) == 'triennially') {
            $mrr_data['monthly_amount'] = $value['amount'] / 36;
            $mrr_data['cycles']         = 36;
            $date                       = $value['mrr_date'];
            for ($i=0; $i <  $mrr_data['cycles']; $i++) {
                  $sql[] = '( 
                            "' .$date.'"                    ,
                            ' .$mrr_data['addon_id']. '   ,
                            '.$mrr_data['monthly_amount'].' ,
                            "'.$mrr_data['booked_date'].'"  ,
                            '.$mrr_data['is_booked'].'                
                    )';

                    $date = date('Y-m-d', strtotime('+30 day', strtotime($date)));
                    $mrr_data['is_booked'] = 1;
            }
        }
    }

    $sql_string = implode($sql,',');
    $query      = 'INSERT IGNORE INTO `mrr_calculation_dates`(date_paid, addon_id, monthly_amount, booked_date, is_booked) VALUES '.$sql_string.'';
    $db         = connect('mrr_addon_reporting');
    $db->query($query);
}

function insertIntoMRRCalculationsDatesTable($data, $sql)
{
    $cycels = isset($data['cycles']) ? $data['cycles'] : 0;
    $date   = isset($data['date']) ? $data['date'] : date('Y-m-d');
    for ($i=0; $i < $cycles; $i++) {
        $sql[] = '( "' .$date.'" , ' .$data['addon_id']. ' , '.$data['monthly_amount'].' )';
        $date = date('Y-m-d', strtotime('+30 day', strtotime($date)));
    }
    return $sql;
}

function execute_query($query)
{

    $db = connect('mrr_addon_reporting');

    $result = $db->query($query);

    $data = [];

    if ($result->num_rows > 0) {

        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
    }
    return $data;
}

function connect($db)
{
    #91.121.47.223
    $servername = '91.121.47.223';
    $username   = 'root';
    $password   = 'gaditek';

    $conn = new mysqli($servername, $username, $password, $db);
    
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
