from customer_profitibility import CustomerProfitability
from datetime import date as dd, datetime as dt, timedelta
from config import configuration as conf

if __name__ == "__main__":
    oCP = CustomerProfitability()
    start_date = '2018-01-01'
    end_date = '2018-12-26'
    start_date = dt.strptime(start_date, '%Y-%m-%d').date()
    end_date = dt.strptime(end_date, '%Y-%m-%d').date()
    diffrence = end_date - start_date

    dates = ['{:%Y-%m-%d}'.format(start_date+timedelta(days=x))
             for x in range(diffrence.days + 1)]
    for date in dates:
        print(oCP.getBillingData(start_date=date) , ' ', date)


