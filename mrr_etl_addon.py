from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

conf  = SparkConf().setAppName("MRR ADDON RAW DATA")
sc    = SparkContext(conf=conf)
spark = SparkSession.builder.config("hive.metastore.uris",
"thrift://hmaster:9084").enableHiveSupport().getOrCreate()

hostname   = 'billing-reporting.ciomqr8vdhn8.eu-central-1.rds.amazonaws.com'
passwd     = 'finance!23#'
start_date = "'2018-01-01'"
end_date   = "'2018-01-30'"

print start_date
print end_date

query = """SELECT 
              DATE(i.`datepaid`) AS invoice_date_paid,
              c.id AS client_id,
              h.id AS hosting_id,
              ah.`id` AS addon_id,
              i.id AS invoice_id,
              LOWER(TRIM(ah.`billingcycle`)) AS addon_cycle,
              LOWER(TRIM(h.`billingcycle`)) AS billing_cycle,
              LOWER(TRIM(it.type)) AS `type`,
              SUBSTRING_INDEX(LOWER(TRIM(it.`description`)), '(', 1) AS item,
              LOWER(TRIM(i.`paymentmethod`)) AS payment_method,
              it.`amount` 
             FROM
              tblclients c 
              JOIN tblinvoices i 
                ON c.`id` = i.`userid` 
              LEFT JOIN tblinvoiceitems it 
                ON it.`invoiceid` = i.`id` 
              LEFT JOIN tblhostingaddons ah 
                ON ah.id = it.`relid` 
              LEFT JOIN tblhosting h 
                ON ah.`hostingid` = h.`id` 
              LEFT JOIN tblaccounts a 
                ON a.`invoiceid` = i.`id` 
             WHERE i.`datepaid` BETWEEN {0} 
              AND {1}
              AND i.`status` = 'Paid' 
              AND it.`type` = 'Addon' 
              AND ah.status IN ('Active', 'Cancelled') 
              AND h.`domainstatus` IN ('Active', 'Cancelled') 
              AND ah.`billingcycle` NOT IN ('Free Account', 'One Time') 
              AND h.`billingcycle` NOT IN ('One Time') 
              AND it.relid > 0 
              AND it.`amount` > 0 
              GROUP BY  i.id
                 """.format(start_date, end_date)

print query

df_billing = spark.read.format("jdbc").options(
    url="jdbc:mysql://%s:3306/billingp_whmcs" % hostname,
    driver="com.mysql.jdbc.Driver",
    dbtable="({}) as t1".format(query),
    user="finance",
    password=passwd).load()

df_billing.createOrReplaceTempView("df_billing")

print df_billing.show(100)

query = "select count(*) from df_billing"

count = spark.sql(query)
print count.show()

df_billing.printSchema()

# JDBC_CS_URL = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format("172.16.1.15", "3306", "churn_segmentation","root","gaditek")

# df_billing \
#     .write.format("jdbc") \
#     .options(url=JDBC_CS_URL, dbtable='arpu_raw_data', driver="com.mysql.jdbc.Driver")\
#     .mode("append") \
#     .save()