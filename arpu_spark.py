from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from config import configuration 

conf  = SparkConf().setAppName("ARPU_taha")
sc    = SparkContext(conf=conf)
spark = SparkSession.builder.config("hive.metastore.uris",
"thrift://hmaster:9084").enableHiveSupport().getOrCreate()

# load database configuration
start_date = '2018-01-01'
end_date = '2018-06-30'

query = """SELECT 
          DATE(i.datepaid) AS date_paid,
          TRIM(LOWER(v.`username`)) AS username,
          i.id AS invoice_id,
          CASE
            WHEN (cfv.`relid` IS NOT NULL) 
            THEN 1 
            ELSE 0 
          END AS is_upgraded,
          it.amount,
          IFNULL(ats.`affiliate_commission`, 0) AS CPA,
          afe.Channel,
          IF(
            ats.affiliate_channel IS NULL 
            OR ats.affiliate_channel = '',
            'N/A',
            ats.affiliate_channel
          ) AS affiliate_channel 
         FROM
          tblinvoices i 
          JOIN tblinvoiceitems it 
            ON it.`invoiceid` = i.`id` 
            AND it.`type` = 'Hosting' 
          JOIN tblvpnusernames v 
            ON v.`hosting_id` = it.relid 
          JOIN tblaccounts a 
            ON a.invoiceid = i.id 
          LEFT JOIN affiliate_transactions ats 
            ON ats.invoice_id = i.id 
          LEFT JOIN affiliatesEmail afe 
            ON afe.`affiliateid` = ats.`affiliate_id` 
          LEFT JOIN tblcustomfieldsvalues AS cfv 
            ON it.relid = cfv.`relid` 
            AND cfv.`fieldid` = 30 
            AND cfv.`relid` <> 0 
         WHERE i.`status` = 'Paid' 
          AND i.datepaid <> '0000-00-00 00:00:00' 
          AND i.datepaid IS NOT NULL 
          AND i.datepaid BETWEEN {0}
          AND {1}
          AND it.`amount` > 0 GROUP BY 1,2,3 """.format(start_date, end_date)

df_billing = spark.read.format("jdbc").options(
    url      = "jdbc:mysql://{}:3306/billingp_whmcs".format(db['billing_readonly'].get('hostname')),
    driver   = "com.mysql.jdbc.Driver",
    dbtable  = """({}) as t1""".format(query) ,
    user     = db['billing_readonly'].get('user'),
    password = db['billing_readonly'].get('password')).load()

df_billing.createOrReplaceTempView("df_billing")

query         = "select * from df_billing group by 1,2,3"
df_billing_v2 = spark.sql("{}".format(query))
print df_billing_v2.show()

query = "select count(*) from df_billing"

count = spark.sql(query)
print count.show()

df_billing.printSchema()

#write data to another db
JDBC_CS_URL = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(db['hmaster'].get('host'), db['hmaster'].get('port'), db['hmaster'].get('db'),db['hmaster'].get('user'),db['hmaster'].get('password'))

df_billing \
    .write.format("jdbc") \
    .options(url=JDBC_CS_URL, dbtable='arpu_raw_data', driver="com.mysql.jdbc.Driver")\
    .mode("append") \
    .save()