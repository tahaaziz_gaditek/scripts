from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from config import configuration
from constants import constants


class Spark:
    def __init__(self, **params):
        Appname = ("App", params.get('app'))[params.get('app') is not None]
        if params.get('debug') is not None and params.get('debug') == True:
            self.conf = SparkConf().setMaster('local').setAppName(Appname)
            self.sc = SparkContext(conf=self.conf)
            self.spark = SparkSession.builder.config(
                "hive.metastore.uris", "thrift://hmaster:9084").enableHiveSupport().getOrCreate()
        else:
            self.conf = SparkConf().setAppName(Appname)
            self.sc = SparkContext(conf=self.conf)
            self.spark = SparkSession.builder.config(
                "hive.metastore.uris", "thrift://hmaster:9084").enableHiveSupport().getOrCreate()
            self.configuration = configuration

    def read_from_sql(self, **params):
        """required: db, query
           optional: driver 
        """
        try:
            if params and params.get('db') and params.get('query') is not None:
                dbconf = params.get('db')
                query = params.get('query')
                driver = (constants['JDBC_DRIVER'], params.get('driver'))[
                    params.get('driver') is not None]
            else:
                raise Exception("Invalid Number of Arguments were sent")

            conf = self.configuration.get(dbconf)

            df_data = self.spark.read \
                .format("jdbc") \
                .options(
                    url="jdbc:mysql://{0}:{2}/{1}".format(
                        conf.get('hostname'), conf.get('database'), conf.get('port')),
                    driver=driver,
                    dbtable="""({0}) as t1""".format(query),
                    user=conf.get('user'),
                    password=conf.get('password')).load()

        except Exception as e:
            return e
        return df_data

    def write_df_to_sql(self, **params):
        """required: db, tablename, df 
           optional: driver, mode
        """
        try:
            if params and params.get('db') and params.get('tablename') and params.get('df'):
                mode = ("append", params.get('mode'))[
                    params.get('mode') is not None]
                driver = ("com.mysql.jdbc.Driver", params.get('driver'))[
                    params.get('driver') is not None]
                dbconf = params.get('db')
                tablename = params.get('tablename')
                df = params.get('df')
            else:
                raise Exception("Invalid Number of Arguments were sent")

            conf = self.configuration.get(dbconf)
            JDBC_CS_URL = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(conf.get(
                'host'), conf.get('port'), conf.get('database'), conf.get('user'), conf.get('password'))

            df.show()
            df \
                .write.format("jdbc") \
                .options(url=JDBC_CS_URL, dbtable=tablename, driver=driver)\
                .mode(mode) \
                .save()
        except Exception as exp:
            return exp
        return df

    def write_df_to_hdfs(self, **params):
        """ required: file_path, paritition, df
            optional: mode(default=append), file_format(default=csv)
        """
        try:
            if params and params.get('df') and params.get('file_path') and param.get('partition') is not None:
                df = self.spark.createDataFrame(params.get('df'))
                df.createOrReplaceTempView("df")
                df.show()
                file_path = params.get('file_path')
                paritition = params.get('partition')
                write_mode = (constants['WRITE_APPEND'], params.get(
                    'mode'))[params.get('mode') is not None]
                file_format = (constants['WRITE_CSV'], params.get('file_format'))[
                    params.get('file_format') is not None]
            else:
                raise Exception("Invalid Number of Arguments were sent")

            df.createOrReplaceTempView("df")

            df.show()
            df \
                .write.format(file_format) \
                .partitionBy(paritition) \
                .mode(write_mode) \
                .save(file_path)

            return df
        except Exception as exp:
            return exp
